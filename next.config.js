/** @type {import('next').NextConfig} */

const baseUrl = "http://172.232.207.203:5371"

const nextConfig = {

    async rewrites() {
        return [
            {
                source: "/api/user/getAuthenticated",
                destination: `${baseUrl}/api/user/getAuthenticated`,
            },
            {
                source: "/api/user/signUp",
                destination: `${baseUrl}/api/user/signUp`,
            },
            {
                source: "/api/user/logIn",
                destination: `${baseUrl}/api/user/logIn`,
            },
            {
                source: "/api/user/logOut",
                destination: `${baseUrl}/api/user/logOut`,
            },
            {
                source: "/api/user/createdQuiz/add",
                destination: `${baseUrl}/api/user/createdQuiz/add`,
            },
            {
                source: "/api/user/assignedQuiz/add",
                destination: `${baseUrl}/api/user/assignedQuiz/add`,
            },
            {
                source: "/api/quiz/create",
                destination: `${baseUrl}/api/quiz/create`,
            },
            {
                source: "/api/question/create",
                destination: `${baseUrl}/api/question/create`,
            },
            {
                source: "/api/answer/create",
                destination: `${baseUrl}/api/answer/create`,
            },
            {
                source: "/api/answer/check",
                destination: `${baseUrl}/api/answer/check`,
            },
            {
                source: "/api/data/getAllForType",
                destination: `${baseUrl}/api/data/getAllForType`,
            },
        ];
    },
}

module.exports = nextConfig
