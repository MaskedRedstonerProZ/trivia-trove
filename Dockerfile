FROM amazoncorretto:19.0.1
EXPOSE 8080:8080
RUN mkdir /app
COPY ./trivia-trove-server.jar /app/trivia-trove-server.jar
ENTRYPOINT ["java","-jar","/app/trivia-trove-server.jar"]