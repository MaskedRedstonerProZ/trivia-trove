import { BadRequestError, ExpectationFailedError, UnauthorizedError } from "@/errors/HttpErrors";
import Question from "@/models/Question";
import Quiz from "@/models/Quiz";
import CreateQuizRequest from "@/models/requests/CreateQuizRequest";
import BasicApiResponse from "@/models/responses/BasicApiResponse";
import QuizResponse from "@/models/responses/QuizResponse";

async function fetchData(input: RequestInfo, init?: RequestInit) {
    const response = await fetch(input, init);
    if (response.ok) {
        return response;
    } else {
        const errorBody = await response.json();
        const errorMessage = errorBody.message;
        if (response.status === 401) {
            throw new UnauthorizedError(errorMessage);
        } else if (response.status === 417) {
            throw new ExpectationFailedError(errorMessage);
        } else if (response.status === 400) {
            throw new BadRequestError(errorMessage)
        } else {
            throw Error("Request failed with status: " + response.status + " message: " + errorMessage);
        }
    }
}

export async function createQuiz(request: CreateQuizRequest): Promise<BasicApiResponse<QuizResponse>> {
    const response = await fetchData("/api/quiz/create", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function getQuestionsForQuiz(quiz: Quiz): Promise<BasicApiResponse<Question[]>> {
    const response = await fetchData(`/api/data/getAllForType/?data_id=${quiz.id}&data_type=question`, { method: "GET" });
    return response.json()
}