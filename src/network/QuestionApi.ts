import { BadRequestError, ExpectationFailedError, UnauthorizedError } from "@/errors/HttpErrors";
import Answer from "@/models/Answer";
import Question from "@/models/Question";
import CreateQuestionRequest from "@/models/requests/CreateQuestionRequest";
import BasicApiResponse from "@/models/responses/BasicApiResponse";
import QuestionResponse from "@/models/responses/QuestionResponse";

async function fetchData(input: RequestInfo, init?: RequestInit) {
    const response = await fetch(input, init);
    if (response.ok) {
        return response;
    } else {
        const errorBody = await response.json();
        const errorMessage = errorBody.message;
        if (response.status === 401) {
            throw new UnauthorizedError(errorMessage);
        } else if (response.status === 417) {
            throw new ExpectationFailedError(errorMessage);
        } else if (response.status === 400) {
            throw new BadRequestError(errorMessage)
        } else {
            throw Error("Request failed with status: " + response.status + " message: " + errorMessage);
        }
    }
}

export async function createQuestion(request: CreateQuestionRequest): Promise<BasicApiResponse<QuestionResponse>> {
    const response = await fetchData("/api/question/create", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function getAnswerForQuestion(question: Question): Promise<BasicApiResponse<Answer>> {
    const response = await fetchData(`/api/data/getAllForType/?data_id=${question.id}&data_type=answer`, { method: "GET" });
    return response.json()
}