import { BadRequestError, ExpectationFailedError, UnauthorizedError } from "@/errors/HttpErrors";
import CheckAnswerRequest from "@/models/requests/CheckAnswerRequest";
import CreateAnswerRequest from "@/models/requests/CreateAnswerRequest";
import AnswerResponse from "@/models/responses/AnswerResponse";
import BasicApiResponse from "@/models/responses/BasicApiResponse";
import CheckAnswerResponse from "@/models/responses/CheckAnswerResponse";

async function fetchData(input: RequestInfo, init?: RequestInit) {
    const response = await fetch(input, init);
    if (response.ok) {
        return response;
    } else {
        const errorBody = await response.json();
        const errorMessage = errorBody.message;
        if (response.status === 401) {
            throw new UnauthorizedError(errorMessage);
        } else if (response.status === 417) {
            throw new ExpectationFailedError(errorMessage);
        } else if (response.status === 400) {
            throw new BadRequestError(errorMessage)
        } else {
            throw Error("Request failed with status: " + response.status + " message: " + errorMessage);
        }
    }
}

export async function createAnswer(request: CreateAnswerRequest): Promise<BasicApiResponse<AnswerResponse>> {
    const response = await fetchData("/api/answer/create", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function checkAnswer(request: CheckAnswerRequest): Promise<BasicApiResponse<CheckAnswerResponse>> {
    const response = await fetchData("/api/answer/check", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}