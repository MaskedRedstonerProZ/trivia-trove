import { BadRequestError, ExpectationFailedError, UnauthorizedError } from "@/errors/HttpErrors";
import Quiz from "@/models/Quiz";
import User from "@/models/User";
import { UserAddAssignedQuizRequest } from "@/models/requests/UserAddAssignedQuizRequest";
import { UserAddCreatedQuizRequest } from "@/models/requests/UserAddCreatedQuizRequest";
import UserLoginRequest from "@/models/requests/UserLoginRequest";
import UserSignUpRequest from "@/models/requests/UserSignUpRequest";
import BasicApiResponse from "@/models/responses/BasicApiResponse";
import WebLoginResponse from "@/models/responses/WebLoginResponse";

async function fetchData(input: RequestInfo, init?: RequestInit) {
    const response = await fetch(input, init);
    if (response.ok) {
        return response;
    } else {
        const errorBody = await response.json();
        const errorMessage = errorBody.message;
        if (response.status === 401) {
            throw new UnauthorizedError(errorMessage);
        } else if (response.status === 417) {
            throw new ExpectationFailedError(errorMessage);
        } else if (response.status === 400) {
            throw new BadRequestError(errorMessage)
        } else {
            throw Error("Request failed with status: " + response.status + " message: " + errorMessage);
        }
    }
}

export async function getLoggedInUser(): Promise<BasicApiResponse<WebLoginResponse>> {
    const response = await fetchData("/api/user/getAuthenticated", { method: "GET" });
    return response.json()
}

export async function getQuizzesForUser(user: User): Promise<BasicApiResponse<Quiz[]>> {
    const response = await fetchData(`/api/data/getAllForType/?user_id=${user.id}&data_type=quiz`, { method: "GET" });
    return response.json()
}

export async function signup(request: UserSignUpRequest): Promise<BasicApiResponse<undefined>> {
    const response = await fetchData("/api/user/signUp", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function login(request: UserLoginRequest): Promise<BasicApiResponse<WebLoginResponse>> {
    const response = await fetchData("/api/user/logIn", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function logout() {
    await fetchData("/api/user/logOut", { method: "DELETE" });
}

export async function addCreatedQuiz(request: UserAddCreatedQuizRequest): Promise<BasicApiResponse<void>> {
    const response = await fetchData("/api/user/createdQuiz/add", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}

export async function addAssignedQuiz(request: UserAddAssignedQuizRequest): Promise<BasicApiResponse<void>> {
    const response = await fetchData("/api/user/assignedQuiz/add", 
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(request)
        }
    )
    return response.json();
}