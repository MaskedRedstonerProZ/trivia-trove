class HttpError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = this.constructor.name;
    }
}

/**
 * Status code: 401
 */
export class UnauthorizedError extends HttpError { }

/**
 * Status code: 417
 */
export class ExpectationFailedError extends HttpError { }

/**
 * Status code: 400
 */
export class BadRequestError extends HttpError { }