
export default interface Question {
    id: string,
    text: string,
    answerId: string 
}