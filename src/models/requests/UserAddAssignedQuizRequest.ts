export interface UserAddAssignedQuizRequest {
    email: string,
    quizId: string
}
