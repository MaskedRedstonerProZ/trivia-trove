export default interface UserLoginRequest {
    platformOfOrigin: string,
    email: string,
    password: string
}