export default interface CreateQuestionRequest {
    text: string,
    answerId: string
}