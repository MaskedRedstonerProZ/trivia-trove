export default interface UserSignUpRequest {
    name: string,
    role: number,
    email: string,
    password: string
}