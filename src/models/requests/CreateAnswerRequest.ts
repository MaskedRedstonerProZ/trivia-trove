import { BlankPosition } from "../Answer";

export default interface CreateAnswerRequest {
    type: string
}

export interface SingleChoice extends CreateAnswerRequest {
    choices: string[],
    correctAnswerChoice: string
}

export interface MultipleChoice extends CreateAnswerRequest {
    choices: string[];
    correctAnswerChoices: string[];
}

export interface TrueFalse extends CreateAnswerRequest {
    isTrue: boolean;
}

export interface FillTheBlanks extends CreateAnswerRequest {
    sentences: string[];
    positionsOfBlanks: BlankPosition[];
}

export interface MatchDescriptions extends CreateAnswerRequest {
    phrases: string[];
    descriptions: string[];
}

export interface Sequencing extends CreateAnswerRequest {
    answerTextsInCorrectSequence: string[];
}