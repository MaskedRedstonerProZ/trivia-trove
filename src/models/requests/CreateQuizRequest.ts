export default interface CreateQuizRequest {
    title: string,
    description: string,
    questionIds: string[]
}