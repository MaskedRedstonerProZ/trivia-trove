import { BlankPosition } from "../Answer";

export default interface CheckAnswerRequest {
    answerId: string
    type: string
}

export interface SingleChoiceCheckAnswerRequest extends CheckAnswerRequest {
    choices: string[],
    correctAnswerChoice: string
}

export interface MultipleChoiceCheckAnswerRequest extends CheckAnswerRequest {
    choices: string[];
    correctAnswerChoices: string[];
}

export interface TrueFalseCheckAnswerRequest extends CheckAnswerRequest {
    isTrue: boolean;
}

export interface FillTheBlanksCheckAnswerRequest extends CheckAnswerRequest {
    sentences: string[];
    positionsOfBlanks: BlankPosition[];
}

export interface MatchDescriptionsCheckAnswerRequest extends CheckAnswerRequest {
    phrases: string[];
    descriptions: string[];
}

export interface SequencingCheckAnswerRequest extends CheckAnswerRequest {
    answerTextsInCorrectSequence: string[];
}