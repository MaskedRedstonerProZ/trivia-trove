export interface UserAddCreatedQuizRequest {
    email: string,
    quizId: string
}
