
export default interface Quiz {
    id: string,
    title: string,
    description: string,
    questionIds: string[]
}