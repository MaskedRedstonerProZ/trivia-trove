export interface BlankPosition {
    first: number,
    second: number
}

export default interface Answer {
    id: string;
}

export interface SingleChoice extends Answer {
    choices: string[];
    correctAnswerChoice: string;
}

export interface MultipleChoice extends Answer {
    choices: string[];
    correctAnswerChoices: string[];
}

export interface TrueFalse extends Answer {
    isTrue: boolean;
}

export interface FillTheBlanks extends Answer {
    sentences: string[];
    positionsOfBlanks: BlankPosition[];
}

export interface MatchDescriptions extends Answer {
    phrases: string[];
    descriptions: string[];
}

export interface Sequencing extends Answer {
    answerTextsInCorrectSequence: string[];
}