import User from "../User";

export default interface WebLoginResponse {
    user: User
}