export default interface CheckAnswerResponse {
    answerId: string,
    isCorrect: boolean
}