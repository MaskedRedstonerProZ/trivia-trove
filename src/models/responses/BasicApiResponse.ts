export default interface BasicApiResponse<T> {
    
    successful: boolean,
    message: string | null,
    data: T | null
}