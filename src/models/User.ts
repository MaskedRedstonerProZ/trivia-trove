
export default interface User {
    id: string,
    name: string,
    role: number,
    email: string,
    password: string,
    createdQuizzes: string[] | null,
    assignedQuizzes: string[] | null
}