"use client"

import styles from "./QuestionListItem.module.css";
import { Card } from "react-bootstrap";
import { ReactNode } from "react";

interface QuestionListItemProps {
    text: string,
    borderColour: string,
    children: ReactNode
}

export default function QuestionListItem({ text, borderColour, children }: QuestionListItemProps) {

    return (
        <Card style={{ borderColor: borderColour, borderStyle: "solid", borderWidth: "5px" } as React.CSSProperties} className={styles.questionCard} data-bs-theme="dark">
            <Card.Header className={styles.questionCardHeader}>
                <Card.Title>
                    {text}
                </Card.Title>
            </Card.Header>
            <Card.Body>
                {children}
            </Card.Body>
        </Card>
    );
}