"use client";

import { ChangeEvent, ReactNode } from "react";
import { Form } from "react-bootstrap";
import { FieldError, RegisterOptions, UseFormRegister } from "react-hook-form";
import styles from "./SelectInputField.module.css";

interface SelectInputFieldProps {
    name: string,
    label?: string,
    register: UseFormRegister<any>,
    registerOptions?: RegisterOptions,
    error?: FieldError,
    onChange?: (e: ChangeEvent<HTMLSelectElement>) => void
    children: ReactNode[]
    [x: string]: any,
}

export default function SelectInputField({ name, label, register, registerOptions, error, onChange, children, ...props }: SelectInputFieldProps) {
    return (
        <Form.Group className="mb-3" controlId={name + "-input"}>
            {label && <Form.Label>{label}:</Form.Label>}
            <div className={styles.selectFieldDiv}>
                <Form.Select
                    className={styles.selectField}
                    data-bs-theme="dark"
                    {...props}
                    {...register(name, registerOptions)}
                    isInvalid={!!error}
                    onChange={onChange}
                >{children}</Form.Select>
            </div>
            <Form.Control.Feedback type="invalid">
                {error?.message}
            </Form.Control.Feedback>
        </Form.Group>
    );
}