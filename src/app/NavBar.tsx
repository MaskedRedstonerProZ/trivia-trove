import User from "@/models/User";
import { Container, Nav, Navbar } from "react-bootstrap";
import NavBarLoggedInView from "./NavBarLoggedInView";
import NavBarLoggedOutView from "./NavBarLoggedOutView";
import Link from "next/link";
import Image from "next/image";
import TriviaTrove from "#/public/TriviaTrove.svg";
import React from "react";

interface NavBarProps {
    loggedInUser: User | null,
    onSignUpClicked: () => void,
    onLoginClicked: () => void,
    onLogoutSuccessful: () => void
}

export default function NavBar({ loggedInUser, onSignUpClicked, onLoginClicked, onLogoutSuccessful }: NavBarProps) {
    return (
        <Navbar bg="black" variant="dark" expand="sm" sticky="top">
            <Container>
                <Navbar.Brand as={Link} href="/">
                    <Image
                        src={TriviaTrove}
                        alt="Logo"
                        style={{ paddingRight: "10px" } as React.CSSProperties}
                        width={45}
                    />
                    TriviaTrove
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="main-navbar" />
                <Navbar.Collapse id="main-navbar">
                    <Nav className="ms-auto">
                        {loggedInUser
                            ? <NavBarLoggedInView user={loggedInUser} onLogoutSuccessful={onLogoutSuccessful} />
                            : <NavBarLoggedOutView onLoginClicked={onLoginClicked} onSignUpClicked={onSignUpClicked} />
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}