"use client";

import Answer, { FillTheBlanks, MatchDescriptions, MultipleChoice, Sequencing, SingleChoice, TrueFalse } from "@/models/Answer";
import Question from "@/models/Question";
import Quiz from "@/models/Quiz";
import User from "@/models/User";
import styles from "./QuizPage.module.css";
import React, { useEffect, useState } from "react";
import CreateQuizModal from "./CreateQuizModal";
import StandardButton from "./StandardButton";
import * as UserApi from "@/network/UserApi";
import * as QuizApi from "@/network/QuizApi";
import * as QuestionApi from "@/network/QuestionApi";
import * as AnswerApi from "@/network/AnswerApi";
import CreateQuestionModal from "./CreateQuestionModal";
import QuestionListItem from "./QuestionListItem";
import TrueFalseAnswer from "./TrueFalseAnswer";
import SingleChoiceAnswer from "./SingleChoiceAnswer";
import MultipleChoiceAnswer from "./MultipleChoiceAnswer";
import FillTheBlanksAnswer from "./FillTheBlanksAnswer";
import MatchDescriptionsAnswer from "./MatchDescriptionsAnswer";
import SequencingAnswer from "./SequencingAnswer";
import CreateQuizRequest from "@/models/requests/CreateQuizRequest";
import { UserRole } from "@/util/UserRole";
import { UserAddCreatedQuizRequest } from "@/models/requests/UserAddCreatedQuizRequest";
import AssignQuizToStudentModal from "./AssignQuizToStudentModal";
import { UserAddAssignedQuizRequest } from "@/models/requests/UserAddAssignedQuizRequest";
import CheckAnswerResponse from "@/models/responses/CheckAnswerResponse";
import CheckAnswerRequest, { FillTheBlanksCheckAnswerRequest, MatchDescriptionsCheckAnswerRequest, MultipleChoiceCheckAnswerRequest, SequencingCheckAnswerRequest, SingleChoiceCheckAnswerRequest, TrueFalseCheckAnswerRequest } from "@/models/requests/CheckAnswerRequest";

interface QuizPageProps {
    loggedInUser: User
}

export default function QuizPage({ loggedInUser }: QuizPageProps) {

    const [showCreateQuizModal, setShowCreateQuizModal] = useState(false);
    const [quizzes, setQuizzes] = useState<Quiz[]>([]);
    const [selectedQuiz, setSelectedQuiz] = useState<Quiz | null>(null);
    const [questions, setQuestions] = useState<Question[]>([]);
    const [answers, setAnswers] = useState<Answer[]>([]);
    const [showCreateQuestionModal, setShowCreateQuestionModal] = useState(false);
    const [showAssignQuizToStudentModal, setShowAssignQuizToStudentModal] = useState(false);
    const [answersToCheck, setAnswersToCheck] = useState<CheckAnswerRequest[]>([]);
    const [didSubmitAnswers, setDidSubmitAnswers] = useState(false);
    const [checkAnswerResponses, setCheckAnswerResponses] = useState<CheckAnswerResponse[]>([])

    function addQuiz(quiz: Quiz) {
        setQuizzes([...quizzes, quiz]);
    }

    function addQuestion(question: Question) {
        setQuestions(questions.concat(question));
    }

    function addAnswer(answer: Answer) {
        setAnswers(answers.concat(answer));
    }

    function addAnswerToCheck(answer: CheckAnswerRequest) {
        setAnswersToCheck([...answersToCheck, answer]);
    }

    function addCheckAnswerResponse(answerResponse: CheckAnswerResponse) {
        setCheckAnswerResponses(prevAnswerResponses => [...prevAnswerResponses, answerResponse]);
    }

    useEffect(() => {
        let isMounted = true;

        async function fetchQuizzes(user: User) {
            try {
                const quizResponse = await UserApi.getQuizzesForUser(user);
                if (isMounted) {
                    quizResponse.data?.length && quizResponse.data?.length !== 0 && quizResponse.data?.forEach((quiz) => {
                        quiz !== null && setQuizzes((prevQuizzes) => [...prevQuizzes, quiz]);
                    });
                }
            } catch (error) {
                alert(error);
            }
        }

        fetchQuizzes(loggedInUser);

        return () => {
            isMounted = false;
        };
    }, [loggedInUser]);

    useEffect(() => {
        async function fetchAnswer(question: Question) {
            try {
                const answerResponse = await QuestionApi.getAnswerForQuestion(question);
                return answerResponse.data;
            } catch (error) {
                alert(error);
                return null;
            }
        }

        async function fetchQuestions(quiz: Quiz) {
            try {
                const questionResponse = await QuizApi.getQuestionsForQuiz(quiz);

                if (questionResponse.data && questionResponse.data.length) {
                    const questionPromises = questionResponse.data.map(async (question) => {
                        const answerData = await fetchAnswer(question);
                        if (answerData !== null) {
                            setAnswers((prevAnswers) => prevAnswers.concat(answerData));
                            setQuestions((prevQuestions) => prevQuestions.concat(question));
                        }
                    });

                    await Promise.all(questionPromises);
                }
            } catch (error) {
                alert(error);
            }
        }

        selectedQuiz && fetchQuestions(selectedQuiz);
    }, [selectedQuiz]);

    useEffect(() => {
        async function checkAnswers() {
            if (didSubmitAnswers) {
                const answerPromises = answersToCheck.map(async (request) => {
                    const answerResponse = await AnswerApi.checkAnswer(request);
                    answerResponse.successful && answerResponse.data && addCheckAnswerResponse(answerResponse.data)
                })
                await Promise.all(answerPromises)
            }
        }
        checkAnswers();
    }, [didSubmitAnswers, answersToCheck]);

    return (
        <>
            {loggedInUser && !selectedQuiz && loggedInUser.role === 0 && <center><StandardButton text="Create new quiz" onClick={() => {
                setShowCreateQuizModal(true);
            }} /></center>}
            {showCreateQuizModal &&
                <CreateQuizModal
                    onDismiss={() => {
                        setShowCreateQuizModal(false);
                    }}
                    onQuizCreated={(request) => {
                        addQuiz({
                            title: request.title,
                            description: request.description,
                            questionIds: request.questionIds
                        } as Quiz)
                        setShowCreateQuizModal(false);
                    }}
                />
            }
            {loggedInUser && !selectedQuiz && quizzes.length !== 0 &&
                quizzes.map((quiz, i) => (
                    <center key={i} className={styles.quizzListItem}>
                        <div onClick={() => {
                            setSelectedQuiz(quizzes[i]);
                        }}>
                            <h1>{quiz.title}</h1>
                            <p>{quiz.description}</p>
                        </div>
                    </center>
                ))
            }
            {selectedQuiz &&
                <center>
                    <h1 style={{ color: "black" } as React.CSSProperties}>{selectedQuiz.title}:</h1>
                    <br />
                    {loggedInUser.role === UserRole.PROFESSOR && <StandardButton text={!loggedInUser.createdQuizzes?.includes(selectedQuiz.id) ? "Create a question" : "Assign this quiz to a student"} onClick={() => {
                        !loggedInUser.createdQuizzes?.includes(selectedQuiz.id) && setShowCreateQuestionModal(true);
                        loggedInUser.createdQuizzes?.includes(selectedQuiz.id) && setShowAssignQuizToStudentModal(true);
                    }} />}
                    {showCreateQuestionModal &&
                        <CreateQuestionModal
                            onDismiss={() => {
                                setShowCreateQuestionModal(false);
                            }}
                            onQuestionCreated={(question, answer) => {
                                addAnswer(answer);
                                addQuestion(question);
                                selectedQuiz.questionIds = selectedQuiz.questionIds ? [...selectedQuiz.questionIds, question.id] : [question.id]
                                setSelectedQuiz(selectedQuiz);
                                setShowCreateQuestionModal(false);
                            }}
                        />
                    }
                    {showAssignQuizToStudentModal &&
                        <AssignQuizToStudentModal
                            onDismiss={() => {
                                setShowAssignQuizToStudentModal(false);
                            }}
                            onQuizAssigned={(request) => {
                                UserApi.addAssignedQuiz(
                                    {
                                        email: request.email,
                                        quizId: selectedQuiz.id
                                    } as UserAddAssignedQuizRequest
                                ).then(() => (
                                    setShowAssignQuizToStudentModal(false)
                                ));
                            }}
                        />
                    }
                    <br />
                    <br />
                    {questions && questions.map((question, i) => {
                        const response = checkAnswerResponses.find((response) => response.answerId === question.answerId)
                        return <React.Fragment key={i}>
                            <QuestionListItem borderColour={response ? (response.isCorrect === true ? "green" : "red") : "transparent"} text={question.text}>
                                {(answers[i] as TrueFalse).isTrue !== undefined &&
                                    <TrueFalseAnswer isTrue={(answers[i] as TrueFalse).isTrue} shouldShowCorrectAnswer={loggedInUser.role === UserRole.PROFESSOR} onCorrectAnswerSelected={(correctAnswer) => {
                                        addAnswerToCheck(
                                            {
                                                isTrue: correctAnswer,
                                                type: "tf",
                                                answerId: answers[i].id
                                            } as TrueFalseCheckAnswerRequest
                                        )
                                    }} />
                                }
                                {(answers[i] as SingleChoice).correctAnswerChoice &&
                                    <SingleChoiceAnswer choices={(answers[i] as SingleChoice).choices} correctAnswerChoice={(answers[i] as SingleChoice).correctAnswerChoice} shouldShowCorrectAnswer={loggedInUser.role === UserRole.PROFESSOR} onCorrectChoiceSelected={(correctChoice) => {
                                        addAnswerToCheck(
                                            {
                                                choices: (answers[i] as SingleChoice).choices,
                                                correctAnswerChoice: correctChoice,
                                                type: "sc",
                                                answerId: answers[i].id
                                            } as SingleChoiceCheckAnswerRequest
                                        )
                                    }} />
                                }
                                {(answers[i] as MultipleChoice).correctAnswerChoices &&
                                    <MultipleChoiceAnswer choices={(answers[i] as MultipleChoice).choices} correctAnswerChoices={(answers[i] as MultipleChoice).correctAnswerChoices} shouldShowCorrectAnswers={loggedInUser.role === UserRole.PROFESSOR} onCorrectChoicesSelected={(correctChoices) => {
                                        addAnswerToCheck(
                                            {
                                                choices: (answers[i] as MultipleChoice).choices,
                                                correctAnswerChoices: correctChoices,
                                                type: "mc",
                                                answerId: answers[i].id
                                            } as MultipleChoiceCheckAnswerRequest
                                        )
                                    }} />
                                }
                                {(answers[i] as FillTheBlanks).positionsOfBlanks &&
                                    <FillTheBlanksAnswer sentences={(answers[i] as FillTheBlanks).sentences} blankPositions={(answers[i] as FillTheBlanks).positionsOfBlanks} shouldShowCorrectAnswer={loggedInUser.role === UserRole.PROFESSOR} onBlanksFilled={(filledSentences) => {
                                        addAnswerToCheck(
                                            {
                                                sentences: filledSentences,
                                                positionsOfBlanks: (answers[i] as FillTheBlanks).positionsOfBlanks,
                                                type: "fb",
                                                answerId: answers[i].id
                                            } as FillTheBlanksCheckAnswerRequest
                                        )
                                    }} />
                                }
                                {(answers[i] as MatchDescriptions).descriptions &&
                                    <MatchDescriptionsAnswer phrases={(answers[i] as MatchDescriptions).phrases} descriptions={(answers[i] as MatchDescriptions).descriptions} shouldShowCorrectAnswer={loggedInUser.role === UserRole.PROFESSOR} onDescriptionsMatched={(descriptions) => {
                                        addAnswerToCheck(
                                            {
                                                phrases: (answers[i] as MatchDescriptions).phrases,
                                                descriptions: descriptions,
                                                type: "md",
                                                answerId: answers[i].id
                                            } as MatchDescriptionsCheckAnswerRequest
                                        )
                                    }} />
                                }
                                {(answers[i] as Sequencing).answerTextsInCorrectSequence &&
                                    <SequencingAnswer answerTexts={(answers[i] as Sequencing).answerTextsInCorrectSequence} shouldShowCorrectAnswer={loggedInUser.role === UserRole.PROFESSOR} onSequenceCompleted={(answerTexts) => {
                                        addAnswerToCheck(
                                            {
                                                answerTextsInCorrectSequence: answerTexts,
                                                type: "sqc",
                                                answerId: answers[i].id
                                            } as SequencingCheckAnswerRequest
                                        )
                                    }} />
                                }
                            </QuestionListItem>
                            <br />
                        </React.Fragment>
                    })}
                    <hr style={{ color: "var(--white)" } as React.CSSProperties} />
                    <StandardButton text="Submit quiz" onClick={async () => {
                        loggedInUser.role !== UserRole.STUDENT && !loggedInUser.createdQuizzes?.includes(selectedQuiz.id) && setQuizzes([...quizzes]);
                        loggedInUser.role !== UserRole.STUDENT && !loggedInUser.createdQuizzes?.includes(selectedQuiz.id) && await QuizApi.createQuiz({
                            title: selectedQuiz.title,
                            description: selectedQuiz.description,
                            questionIds: selectedQuiz.questionIds
                        } as CreateQuizRequest).then(async (quizResponse) => {
                            setSelectedQuiz(null);
                            loggedInUser!!.createdQuizzes = loggedInUser!!.createdQuizzes ? [...loggedInUser!!.createdQuizzes, quizResponse.data?.id!!] : [quizResponse.data?.id!!];
                            await UserApi.addCreatedQuiz(
                                {
                                    email: loggedInUser.email,
                                    quizId: quizResponse.data?.id!!
                                } as UserAddCreatedQuizRequest
                            );
                        })
                        loggedInUser.assignedQuizzes?.includes(selectedQuiz.id) && setDidSubmitAnswers(true);
                    }} />
                </center>
            }
        </>
    );
}
