"use client";

import { useState } from "react";
import styleUtils from "./utils.module.css";
import { Button, Form } from "react-bootstrap";
import TextInputField from "./TextInputField";
import { useForm } from "react-hook-form";

interface SequencingAnswerFormProps {
    onSequenceItemAdded: (sequenceItem: string) => void
}

interface SequenceItem {
    sequenceItemText: string
}

export default function SequencingAnswerForm({ onSequenceItemAdded }: SequencingAnswerFormProps) {

    const [sequenceItems, setSequenceItems] = useState<string[]>([]);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<SequenceItem>();

    return (
        <>
            {sequenceItems.map((sequenceItem, i) => (
                <h4 key={i}>{sequenceItem}</h4>
            ))}
            <Form onSubmit={handleSubmit((sequenceItem) => {
                setSequenceItems([...sequenceItems, sequenceItem.sequenceItemText]);
                onSequenceItemAdded(sequenceItem.sequenceItemText);
            })}>
                <br />
                <TextInputField
                    name="sequenceItemText"
                    type="text"
                    placeholder="Sequence item text"
                    register={register}
                    registerOptions={{ required: "Required" }}
                    error={errors.sequenceItemText}
                />
                <Button
                    type="submit"
                    className={styleUtils.width100}
                    disabled={isSubmitting}
                >
                    Add new sequence item
                </Button>
            </Form>
        </>
    );
}