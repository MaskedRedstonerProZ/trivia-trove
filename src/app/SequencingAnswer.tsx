import { useState } from "react";
import Droppable from "./Droppable";
import Draggable from "./Draggable";
import styleUtils from "./utils.module.css";
import { shuffleArray } from "./Utils";
import StandardButton from "./StandardButton";

interface SequencingAnswerProps {
    answerTexts: string[],
    shouldShowCorrectAnswer: boolean,
    onSequenceCompleted: (answerTexts: string[]) => void
}

interface Container {
    word: string
}

export default function SequencingAnswer({ answerTexts, shouldShowCorrectAnswer, onSequenceCompleted }: SequencingAnswerProps) {

    const containersArray: Container[] = [];

    answerTexts.forEach(() => (
        containersArray.push({ word: "" })
    ));

    const [containers, setContainers] = useState<Container[]>(containersArray);
    const [currentItem, setCurrentItem] = useState("");
    const [currentItemIndex, setCurrentItemIndex] = useState<number>(0);
    const [answerTextSequence, setAnswerTextSequence] = useState<string[]>(shouldShowCorrectAnswer ? answerTexts : shuffleArray(answerTexts));

    const onDrop = (index: number) => {
        containers[currentItemIndex].word = containers[index].word;
        setContainers([...containers]);
        answerTextSequence[currentItemIndex] = answerTextSequence[index];
        answerTextSequence[index] = currentItem
        containers[index].word = currentItem;
        setContainers([...containers]);
        setAnswerTextSequence(answerTextSequence);
    };

    const onDragStart = (item: string, index: number) => {
        setCurrentItem(item);
        setCurrentItemIndex(index);
    };

    const [didSubmit, setDidSubmit] = useState(false);

    return (
        <>
            {answerTextSequence.map((answerText, i) => (
                <Droppable
                    key={i}
                    onDrop={() => onDrop(i)}
                >
                    <Draggable
                        key={answerText}
                        className={styleUtils.disableSelect}
                        onDragStart={(e) => onDragStart(answerText, i)}
                    >
                        <h5>{answerText}</h5>
                    </Draggable>
                </Droppable>
            ))}
            {!shouldShowCorrectAnswer && <br />}
            {!shouldShowCorrectAnswer && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onSequenceCompleted(answerTextSequence); setDidSubmit(true); } } /></center>}
        </>
    );
}