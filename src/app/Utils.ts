import { BlankPosition } from "@/models/Answer";
import UserSignUpRequest from "@/models/requests/UserSignUpRequest";
import { Constants } from "@/util/Constants";


export function makePositionsOfBlanks(sentences: string[]): BlankPosition[] {

    const list: BlankPosition[] = [];

    sentences.forEach((sentence, i) => {
        list.push({ first: sentence.indexOf('(') + 1, second: sentence.indexOf(')') - 1 } as BlankPosition)
    })

    return list;
}

export function shuffleArray<T>(array: T[]): T[] {
    const shuffledArray = [...array];

    for (let i = shuffledArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
    }

    return shuffledArray;
}

export default interface ValidationResult {
    passed: boolean,
    error?: string
}

export function isNumber(char: string): boolean {
    return !isNaN(Number(char));
}

export function isLetter(char: string): boolean {
    return /^[A-Za-z]$/.test(char);
}

export function containsCapitalLetter(str: string): boolean {
    for (let i = 0; i < str.length; i++) {
        if (str[i] >= 'A' && str[i] <= 'Z') {
            return true;
        }
    }
    return false;
}

export function containsNumber(str: string): boolean {
    for (let i = 0; i < str.length; i++) {
        if (str[i] >= '0' && str[i] <= '9') {
            return true;
        }
    }
    return false;
}

export function containsSpecialSymbol(source: string): boolean {

    const specialSymbols = "!\";#$%&'()*+,-./:;<=>?@[]^_`{|}~";

    let flag = false

    for (let i = 0; i < source.length; i++) {
        if (specialSymbols.includes(source[i])) {
            flag = true
        }
    }
    return flag;
}

function validatePassword(password: string): ValidationResult {

    const digitList: string[] = Array.from({ length: 10 }, (_, index) => index.toString());
    const letterList: string[] = Array.from({ length: 26 }, (_, index) => String.fromCharCode("a".charCodeAt(0) + index));
    const capitalLetterList: string[] = Array.from({ length: 26 }, (_, index) => String.fromCharCode("A".charCodeAt(0) + index));

    if (password.length < Constants.MINIMUM_PASSWORD_LENGTH) {
        return { passed: false, error: "The chosen password is too short! Minimum password length is 16 characters." };
    }

    let i = 0;
    let j = 1;
    while (i <= password.length - 2 && i <= password.length - 1) {
        const c = password[i];
        const c1 = password[j];

        if (isNumber(c)) {
            if (digitList.includes(`${c}${c1}`)) {
                return { passed: false, error: "The password is not supposed to have sequential characters such as 123 or abc!" };
            }
        }

        if (isLetter(c)) {
            if (letterList.includes(`${c}${c1}`)) {
                return { passed: false, error: "The password is not supposed to have sequential characters such as 123 or abc!" };
            }
        }

        if (isLetter(c)) {
            if (capitalLetterList.includes(`${c}${c1}`)) {
                return { passed: false, error: "The password is not supposed to have sequential characters such as 123 or abc!" };
            }
        }

        i++;
        j++;
    }

    for (i = 0; i < password.length - 1; i++) {
        for (j = i + 1; j < password.length; j++) {
            if (password[j] === password[i]) {
                return { passed: false, error: "The password is not supposed to contain repeated instances of the same character!" };
            }
        }
    }

    if (!containsCapitalLetter(password)) {
        return { passed: false, error: "The password is supposed to contain at least one capital letter!" }
    }

    if (!containsNumber(password)) {
        return { passed: false, error: "The password is supposed to contain at least one number!" }
    }

    if (!containsSpecialSymbol(password)) {
        return { passed: false, error: "The password is supposed to contain at least one of the following special symbols: ! \" ; # $ % & ' () * + , - . / : ; < = > ? @ [] ^ _ ` { | } ~" }
    }

    return { passed: true }

}

export function validateSignUp(request: UserSignUpRequest): ValidationResult {

    const passwordResult = validatePassword(request.password);

    if (!passwordResult.passed) {
        return passwordResult
    }

    return { passed: true }

}