"use client";

import React from "react";
import { Button } from "react-bootstrap";

interface StandardButtonProps {
    text: string,
    className?: string,
    style?: React.CSSProperties,
    [x: string]: any,
}

export default function StandardButton({ text, className, style, ...props }: StandardButtonProps) {
    return (
        <Button type="button" className="btn btn-primary" style={style} {...props}>{text}</Button>
    );
}