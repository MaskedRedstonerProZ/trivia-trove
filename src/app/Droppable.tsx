import { DragEvent, TouchEvent } from 'react';

interface DroppableProps {
  children?: React.ReactNode;
  className?: string;
  onDrop?: (e: DragEvent<HTMLElement> | TouchEvent<HTMLElement>) => void;
  onDragOver?: (e: DragEvent<HTMLElement> | TouchEvent<HTMLElement>) => void;
}
export default function Droppable(props: DroppableProps) {
  
  const onDrop = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    if (props.onDrop) {
      props.onDrop(e);
    }
  };

  const onTouchEnd = (e: TouchEvent<HTMLElement>) => {
    e.preventDefault();
    if (props.onDrop) {
      props.onDrop(e);
    }
  };

  const onDragOver = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    if (props.onDragOver) {
      props.onDragOver(e);
    }
  };

  const onTouchMove = (e: TouchEvent<HTMLElement>) => {
    e.preventDefault();
    if (props.onDragOver) {
      props.onDragOver(e);
    }
  };


  return (
    <div className={props.className} onDrop={onDrop} onTouchEnd={onTouchEnd} onDragOver={onDragOver} onTouchMove={onTouchMove}>
      {props.children}
    </div>
  );
}
