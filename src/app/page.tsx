"use client"

import User from "@/models/User";
import * as UserApi from "@/network/UserApi";
import React, { useEffect, useState } from "react";
import styles from "./page.module.css";
import NavBar from "./NavBar";
import SignUpModal from "./SignUpModal";
import LoginModal from "./LoginModal";
import QuizPage from "./QuizPage";

export default function Home() {

	const [loggedInUser, setLoggedInUser] = useState<User | null>(null);
	const [showSignUpModal, setShowSignUpModal] = useState(false);
	const [showLogInModal, setShowLogInModal] = useState(false);

	useEffect(() => {
		async function fetchLoggedInUser() {
			try {
				const userResponse = await UserApi.getLoggedInUser();
				if (userResponse.data?.user !== undefined) setLoggedInUser(userResponse.data.user); else throw new Error("User is undefined")
			} catch (error) {
				alert("Please log in to see your quizzes/answer the quizzes you’ve been assigned.")
			}
		}
		fetchLoggedInUser();
	}, []);

	return (
		<div>
			<NavBar
				loggedInUser={loggedInUser}
				onSignUpClicked={() => {
					setShowSignUpModal(true);
				}}
				onLoginClicked={() => {
					setShowLogInModal(true);
				}}
				onLogoutSuccessful={() => {
					setLoggedInUser(null);
				}}
			/>
			{showSignUpModal &&
				<SignUpModal
					onDismiss={() => {
						setShowSignUpModal(false);
					}}
					onSignUpSuccessful={() => {
						setShowSignUpModal(false);
					}}
				/>
			}
			{showLogInModal &&
				<LoginModal
					onDismiss={() => {
						setShowLogInModal(false);
					}}
					onLoginSuccessful={(user) => {
						setLoggedInUser(user);
						setShowLogInModal(false);
					}}
				/>
			}
			{!loggedInUser &&
				<center className={styles.loggedOutMessage}><p>Please log in to see your quizzes/answer the quizzes you’ve been assigned.</p></center>
			}
			<br />
			{loggedInUser && <QuizPage loggedInUser={loggedInUser} />}
		</div>
	)
}
