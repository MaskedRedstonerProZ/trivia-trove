"use client";

import { BlankPosition } from "@/models/Answer";
import styleUtils from "./utils.module.css";
import React, { useState } from "react";
import Draggable from "./Draggable";
import Droppable from "./Droppable";
import StandardButton from "./StandardButton";

interface FillTheBlanksAnswerProps {
    sentences: string[],
    blankPositions: BlankPosition[],
    shouldShowCorrectAnswer: boolean,
    onBlanksFilled: (filledSentences: string[]) => void
}

interface Container {
    name: string,
    word: string
}

export default function FillTheBlanksAnswer({ sentences, blankPositions, shouldShowCorrectAnswer, onBlanksFilled }: FillTheBlanksAnswerProps) {

    const [potentialCorrectAnswers, setPotentialCorrectAnswers] = useState<string[]>([])
    const [didSubmit, setDidSubmit] = useState(false);

    function addPotentialCorrectAnswer(potentialCorrectAnswer: string) {
        setPotentialCorrectAnswers([...potentialCorrectAnswers, potentialCorrectAnswer]);
    }

    const modifiedSentences: string[] = [];
    let replacedWords: string[] = [];
    const blanks: string[] = []

    sentences.forEach((sentence, i) => {
        let modifiedSentence = sentence;
        let wordToReplace: string;

        const blankPosition = blankPositions[i];

        wordToReplace = sentence.substring(blankPosition.first, blankPosition.second + 1);

        let blank = "";

        for (let i = 0; i < wordToReplace.length; i++) {
            blank += "_"
        }

        modifiedSentence = modifiedSentence.replace(wordToReplace, blank);
        blanks.push(blank);
        replacedWords.push(wordToReplace);

        modifiedSentences.push(modifiedSentence);
    });

    const containersArray: Container[] = []

    replacedWords.forEach((replacedWord, i) => (
        containersArray.push({ name: blanks[i], word: shouldShowCorrectAnswer ? replacedWord : ""})
    ));

    const [containers, setContainers] = useState<Container[]>(containersArray);
    const [currentItem, setCurrentItem] = useState<string>("");
    const [fillerWords, setFillerWords] = useState<string[]>(replacedWords);

    const onDrop = (index: number) => {
        containers[index].word = currentItem;
        setContainers([...containers]);
        setFillerWords(fillerWords.filter((value) => value !== currentItem));
        addPotentialCorrectAnswer(`${modifiedSentences[index].substring(0, modifiedSentences[index].indexOf('_'))}${containers[index].word}${modifiedSentences[index].substring(modifiedSentences[index].lastIndexOf('_') + 1)}`);
    };

    const onDragStart = (item: string) => {
        setCurrentItem(item);
    };

    return (
        <>
            {modifiedSentences.map((sentence, i) => (
                <div key={i}>
                    <Droppable
                        key={i}
                        onDrop={() => onDrop(i)}
                    >
                        {containers[i].word == "" ? (
                            <span>{sentence.substring(0, sentence.indexOf('_'))}{containers[i].name}{sentence.substring(sentence.lastIndexOf('_') + 1)}</span>
                        ) : (
                            <span key={containers[i].word}>{sentence.substring(0, sentence.indexOf('_'))}{<span style={{ opacity: "80%" } as React.CSSProperties} >{containers[i].word}</span>}{sentence.substring(sentence.lastIndexOf('_') + 1)}</span>
                        )
                        }
                    </Droppable>
                </div>
            ))}
            {!shouldShowCorrectAnswer && 
                <>
                    <br />
                    <div className={styleUtils.flexCenter} >
                        {fillerWords.map((fillerWord) => (
                            <Draggable
                                key={fillerWord}
                                className={styleUtils.disableSelect}
                                onDragStart={(e) => onDragStart(fillerWord)}
                            >
                                {fillerWord}
                            </Draggable>
                        ))}
                    </div>
                    <br />
                </>
            }
            {!shouldShowCorrectAnswer && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onBlanksFilled(potentialCorrectAnswers); setDidSubmit(true); } } /></center>}
        </>
    );
}