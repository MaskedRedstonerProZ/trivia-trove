"use client";

import { useState } from "react";
import styleUtils from "./utils.module.css";
import { Button, Form } from "react-bootstrap";
import TextInputField from "./TextInputField";
import { useForm } from "react-hook-form";

interface MatchDescriptionsAnswerFormProps {
    onPhraseDescriptionPairAdded: (matchDescriptionsItem: MatchDescriptionsItem) => void
}

interface MatchDescriptionsItem {
    phrase: string,
    description: string
}

export default function MatchDescriptionsAnswerForm({ onPhraseDescriptionPairAdded }: MatchDescriptionsAnswerFormProps) {

    const [matchDescriptionsItems, setMatchDescriptionsItems] = useState<MatchDescriptionsItem[]>([]);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<MatchDescriptionsItem>();

    return (
        <>
            {matchDescriptionsItems.map((matchDescriptionsItem, i) => (
                <div className={styleUtils.flex} key={i}>
                    <h4>{matchDescriptionsItem.phrase} - {matchDescriptionsItem.description}</h4>
                </div>
            ))}
            <Form onSubmit={handleSubmit((matchDescriptionItem) => {
                setMatchDescriptionsItems([...matchDescriptionsItems, matchDescriptionItem]);
                onPhraseDescriptionPairAdded(matchDescriptionItem);
            })}>
                <br />
                <div className={styleUtils.flex}>
                    <TextInputField
                        name="phrase"
                        type="text"
                        placeholder="Phrase"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.phrase}
                    />
                    <h1>-</h1>
                    <TextInputField
                        name="description"
                        type="text"
                        placeholder="Description"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.description}
                    />
                </div>
                <Button
                    type="submit"
                    className={styleUtils.width100}
                    disabled={isSubmitting}
                >
                    Add new phrase and description
                </Button>
            </Form>
        </>
    );
}