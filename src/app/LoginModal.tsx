"use client";

import { useForm } from "react-hook-form";
import User from "@/models/User";
import * as UserApi from "@/network/UserApi";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import TextInputField from "./TextInputField";
import styleUtils from "./utils.module.css";
import { useState } from 'react';
import { UnauthorizedError } from "@/errors/HttpErrors";
import UserLoginRequest from "@/models/requests/UserLoginRequest";

interface LoginModalProps {
    onDismiss: () => void,
    onLoginSuccessful: (user: User) => void,
}

export default function LoginModal({ onDismiss, onLoginSuccessful }: LoginModalProps) {

    const [errorText, setErrorText] = useState<string | null>(null);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<UserLoginRequest>();

    async function onSubmit(request: UserLoginRequest) {
        try {
            request.platformOfOrigin = "Web"
            const userResponse = await UserApi.login(request);
            if(userResponse.data?.user !== undefined) {
                onLoginSuccessful(userResponse.data?.user);
            } else setErrorText(userResponse.message)
        } catch (error) {
            if (error instanceof UnauthorizedError) {
                setErrorText(error.message);
            } else {
                alert(error);
            }
        }
    }

    return (
        <Modal show onHide={onDismiss}>
            <Modal.Header data-bs-theme="dark" closeButton>
                <Modal.Title>
                    Log In
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {errorText &&
                    <Alert variant="danger">
                        {errorText}
                    </Alert>
                }
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <TextInputField
                        name="email"
                        label="Email"
                        type="text"
                        placeholder="Email"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.email}
                    />
                    <TextInputField
                        name="password"
                        label="Password"
                        type="password"
                        placeholder="Password"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.password}
                    />
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        className={styleUtils.width100}>
                        Log In
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}