
import { Button, Navbar } from "react-bootstrap";
import User from "@/models/User";
import * as UserApi from "@/network/UserApi";

interface NavBarLoggedInViewProps {
    user: User,
    onLogoutSuccessful: () => void,
}

export default function NavBarLoggedInView({ user, onLogoutSuccessful }: NavBarLoggedInViewProps) {

    const buttonStyle = { backgroundColor: "black", borderColor: "black" } as React.CSSProperties

    async function logout() {
        try {
            await UserApi.logout();
            onLogoutSuccessful();
        } catch (error) {
            alert(error);
        }
    }

    return (
        <>
            <Navbar.Text className="me-2">
                Hello, {user.name}
            </Navbar.Text>
            <Button style={buttonStyle} onClick={logout}>Log out</Button>
        </>
    );
}