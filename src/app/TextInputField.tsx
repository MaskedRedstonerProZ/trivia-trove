"use client";

import { Form } from "react-bootstrap";
import { FieldError, RegisterOptions, UseFormRegister } from "react-hook-form";
import styles from "./TextInputField.module.css";

interface TextInputFieldProps {
    name: string,
    label?: string,
    register: UseFormRegister<any>,
    registerOptions?: RegisterOptions,
    error?: FieldError,
    value?: string
    [x: string]: any,
}

export default function TextInputField({ name, label, register, registerOptions, error, value, ...props }: TextInputFieldProps) {
    return (
        <Form.Group className="mb-3" controlId={name + "-input"}>
            {label && <Form.Label>{label}:</Form.Label>}
            <div className={styles.textFieldDiv}>
                <Form.Control
                    value={(value !== undefined && value !== "") ? value : undefined}
                    {...props}
                    {...register(name, registerOptions)}
                    isInvalid={!!error}
                />
                <Form.Control.Feedback type="invalid">
                    {error?.message}
                </Form.Control.Feedback>
            </div>
        </Form.Group>
    );
}