import { useState } from "react";
import Droppable from "./Droppable";
import Draggable from "./Draggable";
import styleUtils from "./utils.module.css";
import MatchDescriptionsAnswerArrow from "#/public/MatchDescriptionsAnswerArrow.svg";
import Image from "next/image";
import { shuffleArray } from "./Utils";
import StandardButton from "./StandardButton";

interface MatchDescriptionsAnswerProps {
    phrases: string[],
    descriptions: string[],
    shouldShowCorrectAnswer: boolean,
    onDescriptionsMatched: (descriptions: string[]) => void
}

interface Container {
    word: string
}

export default function MatchDescriptionsAnswer({ phrases, descriptions, shouldShowCorrectAnswer, onDescriptionsMatched }: MatchDescriptionsAnswerProps) {

    const containersArray: Container[] = [];

    descriptions.forEach(() => (
        containersArray.push({ word: "" })
    ));

    const [containers, setContainers] = useState<Container[]>(containersArray);
    const [currentItem, setCurrentItem] = useState("");
    const [currentItemIndex, setCurrentItemIndex] = useState<number>(0);
    const [phraseDescriptions, setPhraseDescriptions] = useState<string[]>(shouldShowCorrectAnswer ? descriptions : shuffleArray(descriptions));

    const onDrop = (index: number) => {
        containers[currentItemIndex].word = containers[index].word;
        setContainers([...containers]);
        phraseDescriptions[currentItemIndex] = phraseDescriptions[index];
        phraseDescriptions[index] = currentItem
        containers[index].word = currentItem;
        setContainers([...containers]);
        setPhraseDescriptions(phraseDescriptions);
    };

    const onDragStart = (item: string, index: number) => {
        setCurrentItem(item);
        setCurrentItemIndex(index);
    };

    const [didSubmit, setDidSubmit] = useState(false);

    return (
        <>
            {phrases.map((phrase, i) => (
                <div key={i} className={styleUtils.flex} >
                    <h5>{phrase}</h5>
                    <Image
                        src={MatchDescriptionsAnswerArrow}
                        alt="Match descriptions answer arrow"
                        height={32}
                        width={95}
                    />
                    <Droppable
                        key={i}
                        onDrop={() => onDrop(i)}
                    >
                        <Draggable
                            key={phraseDescriptions[i]}
                            className={styleUtils.disableSelect}
                            onDragStart={() => onDragStart(phraseDescriptions[i], i)}
                        >
                            <h5>{phraseDescriptions[i]}</h5>
                        </Draggable>
                    </Droppable>
                </div>
            ))}
            {!shouldShowCorrectAnswer && <br />}
            {!shouldShowCorrectAnswer && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onDescriptionsMatched(phraseDescriptions); setDidSubmit(true); } } /></center>}
        </>
    );
}