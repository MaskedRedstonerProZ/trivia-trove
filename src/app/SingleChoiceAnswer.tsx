"use client";

import { useState } from "react";
import { Form } from "react-bootstrap";
import StandardButton from "./StandardButton";

interface SingleChoiceAnswerProps {
    choices: string[],
    correctAnswerChoice?: string,
    shouldShowCorrectAnswer: boolean,
    onCorrectChoiceSelected: (correctChoice: string) => void
}

export default function SingleChoiceAnswer({ choices, correctAnswerChoice, shouldShowCorrectAnswer, onCorrectChoiceSelected }: SingleChoiceAnswerProps) {

    const [selectedIndex, setSelectedIndex] = useState(0);
    const [didSubmit, setDidSubmit] = useState(false);

    return (
        <>
            {shouldShowCorrectAnswer ? 
                choices.map((choice, i) => (
                    <Form.Check
                        checked={choice === correctAnswerChoice}
                        readOnly={choice === correctAnswerChoice}
                        label={choice}
                        name="sc-answer"
                        type="radio"
                        key={i}
                    />
                ))
                :
                choices.map((choice, i) => (
                    <Form.Check
                        label={choice}
                        defaultChecked={false}
                        name="sc-answer"
                        type="radio"
                        key={i}
                        onClick={() => {
                            setSelectedIndex(i);
                        }}
                    />
                ))
            }
            {!shouldShowCorrectAnswer && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onCorrectChoiceSelected(choices[selectedIndex]); setDidSubmit(true); } } /></center>}
        </>
    );
}