"use client";

import { useForm } from "react-hook-form";
import { Button, Form, Modal } from "react-bootstrap";
import TextInputField from "./TextInputField";
import styleUtils from "./utils.module.css";
import CreateQuizRequest from "@/models/requests/CreateQuizRequest";

interface CreateQuizModalProps {
    onDismiss: () => void,
    onQuizCreated: (request: CreateQuizRequest) => void,
}

export default function CreateQuizModal({ onDismiss, onQuizCreated }: CreateQuizModalProps) {

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<CreateQuizRequest>();

    return (
        <Modal show onHide={onDismiss}>
            <Modal.Header data-bs-theme="dark" closeButton>
                <Modal.Title>
                    Create Quiz
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form onSubmit={handleSubmit(onQuizCreated)}>
                    <TextInputField
                        name="title"
                        label="Title"
                        type="text"
                        placeholder="Title"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.title}
                    />
                    <TextInputField
                        name="description"
                        label="Description"
                        type="text"
                        as="textarea"
                        placeholder="Description"
                        register={register}
                        error={errors.description}
                        rows={5}
                    />
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        className={styleUtils.width100}>
                        Create Quiz
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}