"use client";

import { useForm } from "react-hook-form";
import { Button, Form, Modal } from "react-bootstrap";
import TextInputField from "./TextInputField";
import styleUtils from "./utils.module.css";
import { UserAddAssignedQuizRequest } from "@/models/requests/UserAddAssignedQuizRequest";

interface AssignQuizToStudentModalProps {
    onDismiss: () => void,
    onQuizAssigned: (request: UserAddAssignedQuizRequest) => void,
}

export default function AssignQuizToStudentModal({ onDismiss, onQuizAssigned }: AssignQuizToStudentModalProps) {

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<UserAddAssignedQuizRequest>();

    return (
        <Modal show onHide={onDismiss}>
            <Modal.Header data-bs-theme="dark" closeButton>
                <Modal.Title>
                    Assign Quiz to student
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form onSubmit={handleSubmit(onQuizAssigned)}>
                    <TextInputField
                        name="email"
                        label="Student e-mail"
                        type="text"
                        placeholder="E-mail"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.email}
                    />
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        className={styleUtils.width100}>
                        Assign Quiz
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}