"use client";

import styleUtils from "./utils.module.css";
import { Button, Form } from "react-bootstrap";
import { useState } from "react";

interface TrueFalseAnswerFormProps {
    onAnswerAdded: (isTrue: boolean) => void
}

export default function TrueFalseAnswerForm({ onAnswerAdded }: TrueFalseAnswerFormProps) {

    const [isTrue, setIsTrue] = useState(false);

    return (
        <>
            <Form>
                <br />
                <Form.Check
                    data-bs-theme="dark"
                    label={<h4 className={styleUtils.disableSelect}>Is true</h4>}
                    name="isTrue"
                    type="checkbox"
                    onClick={(e) => {
                        setIsTrue(!isTrue);
                    }}
                />
                <br />
                <Button
                    className={styleUtils.width100}
                    onClick={() => {
                        onAnswerAdded(isTrue);
                    }}
                >
                    Add new answer
                </Button>
            </Form>
        </>
    );
}