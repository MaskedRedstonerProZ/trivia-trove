import "bootstrap/dist/css/bootstrap.min.css";
import './globals.css'
import type { Metadata } from 'next'
import { Orbitron } from 'next/font/google'

const orbitron = Orbitron({ subsets: ['latin'], weight: "900" })

export const metadata: Metadata = {
  title: 'TriviaTrove',
  description: 'TriviaTrove is a quiz app deveoped by MaskedRedstonerProZ',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {

  return (
    <html lang="en">
      <body className={orbitron.className}>
        <main>
          {children}
        </main>
      </body>
    </html>
  )
}
