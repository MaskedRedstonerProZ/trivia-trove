import React, { DragEvent, TouchEvent } from 'react';

interface DraggableProps {
  children?: React.ReactNode;
  disabled?: boolean 
  className?: string;
  onDragStart?: (e: DragEvent<HTMLElement> | TouchEvent<HTMLElement>) => void;
}
export default function Draggable(props: DraggableProps) {
  const onDragStart = (e: DragEvent<HTMLElement>) => {
    if (props.onDragStart) {
      props.onDragStart(e);
    }
  };

  const onTouchStart = (e: TouchEvent<HTMLElement>) => {
    if (props.onDragStart) {
      props.onDragStart(e);
    }
  }

  return (
    <div className={props.className} style={{ cursor: "pointer" } as React.CSSProperties} onTouchStart={onTouchStart} onDragStart={onDragStart} draggable={!props.disabled}>
      {props.children}
    </div>
  );
}
