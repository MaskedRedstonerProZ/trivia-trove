"use client";

import { Form } from "react-bootstrap";
import StandardButton from "./StandardButton";
import { useState } from "react";

interface TrueFalseAnswerProps {
    isTrue: boolean,
    shouldShowCorrectAnswer: boolean,
    onCorrectAnswerSelected: (correctAnswer: boolean) => void
}

export default function TrueFalseAnswer({ isTrue, shouldShowCorrectAnswer, onCorrectAnswerSelected }: TrueFalseAnswerProps) {
    
    const [correctAnswer, setCorrectAnswer] = useState(false);
    const [didSubmit, setDidSubmit] = useState(false);

    return (
        <div>
            {shouldShowCorrectAnswer ? 
                <>
                    <Form.Check
                        checked={isTrue}
                        readOnly={isTrue}
                        label="True"
                        name="tf-answer"
                        type="radio"
                    />
                    <Form.Check
                        checked={!isTrue}
                        readOnly={!isTrue}
                        label="False"
                        name="tf-answer"
                        type="radio"
                    />
                </>
                :
                <>
                    <Form.Check
                        label="True"
                        defaultChecked={false}
                        name="tf-answer"
                        type="radio"
                        onChange={() => {
                            setCorrectAnswer(true);
                        }}
                    />
                    <Form.Check
                        label="False"
                        defaultChecked={false}
                        name="tf-answer"
                        type="radio"
                        onChange={() => {
                            setCorrectAnswer(false);
                        }}
                    />
                </>
            }
{!shouldShowCorrectAnswer && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onCorrectAnswerSelected(correctAnswer); setDidSubmit(true); } } /></center>}        </div>
    );
}