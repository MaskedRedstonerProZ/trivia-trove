
import React from "react";
import { Button } from "react-bootstrap";

interface NavBarLoggedOutViewProps {
    onSignUpClicked: () => void,
    onLoginClicked: () => void,
}

export default function NavBarLoggedOutView({ onSignUpClicked, onLoginClicked }: NavBarLoggedOutViewProps) {

    const buttonStyle = { backgroundColor: "black", borderColor: "black" } as React.CSSProperties

    return (
        <>
            <Button style={buttonStyle} onClick={onSignUpClicked}>Sign Up</Button>
            <Button style={buttonStyle} onClick={onLoginClicked}>Log In</Button>
        </>
    );
}