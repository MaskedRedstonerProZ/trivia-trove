"use client"

import { useForm } from "react-hook-form";
import UserSignUpRequest from "@/models/requests/UserSignUpRequest";
import * as UserApi from "@/network/UserApi";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import TextInputField from "./TextInputField";
import styleUtils from "./utils.module.css";
import { useState } from 'react';
import { ExpectationFailedError } from "@/errors/HttpErrors";
import SelectInputField from "./SelectInputField";
import { validateSignUp } from "./Utils";

interface SignUpModalProps {
    onDismiss: () => void,
    onSignUpSuccessful: () => void
}

export default function SignUpModal({ onDismiss, onSignUpSuccessful }: SignUpModalProps) {

    const [errorText, setErrorText] = useState<string | null>(null);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<UserSignUpRequest>();

    async function onSubmit(request: UserSignUpRequest) {

        const validationResult = validateSignUp(request);

        if(validationResult.passed) {
            try {
                await UserApi.signup(request);
                onSignUpSuccessful();
            } catch (error) {
                if (error instanceof ExpectationFailedError) {
                    setErrorText(error.message);
                } else {
                    alert(error);
                }
            }
        } else setErrorText(validationResult?.error!!)
    }

    return (
        <Modal show onHide={onDismiss}>
            <Modal.Header data-bs-theme="dark" closeButton>
                <Modal.Title>
                    Sign Up
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {errorText &&
                    <Alert variant="danger">
                        {errorText}
                    </Alert>
                }
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <TextInputField
                        name="name"
                        label="Username"
                        type="text"
                        placeholder="Username"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.name}
                    />
                    <SelectInputField
                        name="role"
                        label="Role"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.role}
                    >
                        <option value="0">Professor</option>
                        <option value="1">Student</option>
                    </SelectInputField>
                    <TextInputField
                        name="email"
                        label="Email"
                        type="email"
                        placeholder="Email"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.email}
                    />
                    <TextInputField
                        name="password"
                        label="Password"
                        type="password"
                        placeholder="Password"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.password}
                    />
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        className={styleUtils.width100}>
                        Sign Up
                    </Button>
                </Form>
            </Modal.Body>

        </Modal>
    );
}