"use client";

import { useState } from "react";
import styleUtils from "./utils.module.css";
import { Button, Form } from "react-bootstrap";
import TextInputField from "./TextInputField";
import { useForm } from "react-hook-form";

interface FillTheBlanksAnswerFormProps {
    onSentenceItemAdded: (sentenceItem: string) => void
}

interface SentenceItem {
    sentenceItemText: string
}

export default function FillTheBlanksAnswerForm({ onSentenceItemAdded }: FillTheBlanksAnswerFormProps) {

    const [sentenceItems, setSentenceItems] = useState<string[]>([]);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<SentenceItem>();

    return (
        <>
            {sentenceItems.map((sequenceItem, i) => (
                <h4 key={i} >{sequenceItem}</h4>
            ))}
            <Form onSubmit={handleSubmit((sentenceItem) => {
                setSentenceItems([...sentenceItems, sentenceItem.sentenceItemText]);
                onSentenceItemAdded(sentenceItem.sentenceItemText);
            })}>
                <br />
                <TextInputField
                    name="sentenceItemText"
                    type="text"
                    placeholder="Sentence with blank filler in brackets (like-this)"
                    register={register}
                    registerOptions={{ required: "Required" }}
                    error={errors.sentenceItemText}
                />
                <Button
                    type="submit"
                    className={styleUtils.width100}
                    disabled={isSubmitting}
                >
                    Add new sentence
                </Button>
            </Form>
        </>
    );
}