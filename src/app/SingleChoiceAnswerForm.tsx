"use client";

import { useState } from "react";
import styleUtils from "./utils.module.css";
import { Button, Form } from "react-bootstrap";
import TextInputField from "./TextInputField";
import { useForm } from "react-hook-form";

interface SingleChoiceAnswerFormProps {
    onChoiceAdded: (choice: string) => void,
    onCorrectChoiceAdded: (correctChoice: string) => void
}

interface Choice {
    choiceText: string
}

export default function SingleChoiceAnswerForm({ onChoiceAdded, onCorrectChoiceAdded }: SingleChoiceAnswerFormProps) {

    const [choices, setChoices] = useState<string[]>([]);

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<Choice>();

    return (
        <>
            {choices.map((choice, i) => (
                <Form.Check
                    data-bs-theme="dark"
                    label={<h4>{choice}</h4>}
                    name="sc-answer-form"
                    type="radio"
                    key={i}
                    onChange={(e) => {
                        onCorrectChoiceAdded(choice);
                    }}
                />
            ))}
            <Form onSubmit={handleSubmit((choice) => {
                setChoices([...choices, choice.choiceText]);
                onChoiceAdded(choice.choiceText);
            })}>
                <br />
                <TextInputField
                    name="choiceText"
                    type="text"
                    placeholder="Choice Text"
                    register={register}
                    registerOptions={{ required: "Required" }}
                    error={errors.choiceText}
                />
                <Button
                    type="submit"
                    className={styleUtils.width100}
                    disabled={isSubmitting}
                >
                    Add new choice
                </Button>
            </Form>
        </>
    );
}