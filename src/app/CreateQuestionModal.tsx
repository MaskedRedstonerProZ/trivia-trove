"use client";

import { useForm } from "react-hook-form";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import TextInputField from "./TextInputField";
import styleUtils from "./utils.module.css";
import * as AnswerApi from "@/network/AnswerApi";
import * as QuestionApi from "@/network/QuestionApi";
import CreateQuestionRequest from "@/models/requests/CreateQuestionRequest";
import { useState } from "react";
import Answer, { SingleChoice, MultipleChoice, TrueFalse, Sequencing, MatchDescriptions, FillTheBlanks } from "@/models/Answer";
import { 
    Sequencing as SequencingAnswerRequest,
    MatchDescriptions as MatchDescriptionsAnswerRequest,
    FillTheBlanks as FillTheBlanksAnswerRequest,
    TrueFalse as TrueFalseAnswerRequest, 
    MultipleChoice as MultipleChoiceAnswerRequest, 
    SingleChoice as SingleChoiceAnswerRequest 
} from "@/models/requests/CreateAnswerRequest";
import AnswerTypeSelector from "./AnswerTypeSelector";
import SingleChoiceAnswerForm from "./SingleChoiceAnswerForm";
import MultipleChoiceAnswerForm from "./MultipleChoiceAnswerForm";
import TrueFalseAnswerForm from "./TrueFalseAnswerForm";
import SequencingAnswerForm from "./SequencingAnswerForm";
import MatchDescriptionsAnswerForm from "./MatchDescriptionsAnswerForm";
import FillTheBlanksAnswerForm from "./FillTheBlanksAnswerForm";
import { makePositionsOfBlanks } from "./Utils";
import Question from "@/models/Question";
import { ExpectationFailedError } from "@/errors/HttpErrors";

interface CreateQuestionModalProps {
    onDismiss: () => void,
    onQuestionCreated: (question: Question, answer: Answer) => void,
}

export default function CreateQuestionModal({ onDismiss, onQuestionCreated }: CreateQuestionModalProps) {

    const [answer, setAnswer] = useState<Answer>();
    const [answerType, setAnswerType] = useState("sc");
    const [shouldShowError, setShouldShowError] = useState(false);
    const [choices, setChoices] = useState<string[]>([]);
    const [isAnswerTypeSelectorDisabled, setIsAnswerTypeSelectorDisabled] = useState(false);
    const [errorText, setErrorText] = useState<string>("The Answer data must be created before moving on with the Question");

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<CreateQuestionRequest>();

    async function createAnswer(answer: Answer): Promise<string> {
        let responseString = ""; 
        (answer as SingleChoice).correctAnswerChoice && await AnswerApi.createAnswer(
            {
                choices: (answer as SingleChoice).choices,
                correctAnswerChoice: (answer as SingleChoice).correctAnswerChoice,
                type: "sc"
            } as SingleChoiceAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        (answer as MultipleChoice).correctAnswerChoices && await AnswerApi.createAnswer(
            {
                choices: (answer as MultipleChoice).choices,
                correctAnswerChoices: (answer as MultipleChoice).correctAnswerChoices,
                type: "mc"
            } as MultipleChoiceAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        (answer as TrueFalse).isTrue !== undefined && await AnswerApi.createAnswer(
            {
                isTrue: (answer as TrueFalse).isTrue,
                type: "tf"
            } as TrueFalseAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        (answer as FillTheBlanks).sentences && await AnswerApi.createAnswer(
            {
                sentences: (answer as FillTheBlanks).sentences,
                positionsOfBlanks: (answer as FillTheBlanks).positionsOfBlanks,
                type: "fb"
            } as FillTheBlanksAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        (answer as MatchDescriptions).descriptions && await AnswerApi.createAnswer(
            {
                phrases: (answer as MatchDescriptions).phrases,
                descriptions: (answer as MatchDescriptions).descriptions,
                type: "md"
            } as MatchDescriptionsAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        (answer as Sequencing).answerTextsInCorrectSequence && await AnswerApi.createAnswer(
            {
                answerTextsInCorrectSequence: (answer as Sequencing).answerTextsInCorrectSequence,
                type: "sqc"
            } as SequencingAnswerRequest
        ).then((response) => {
            responseString = response.data?.id!!;
        });
        return responseString;
    }

    const onSubmit = async (request: CreateQuestionRequest) => {
        if(answer !== undefined) {
            await createAnswer(answer).then(async (answerId) => {
                try {
                    await QuestionApi.createQuestion(
                        {
                            text: answerType === "fb" ? "Fill in the blanks" : answerType === "md" ? "Match the phrases with their descriptions" : answerType === "sqc" ? "Put the sentences in the correct sequence" : request.text,
                            answerId: answerId
                        } as CreateQuestionRequest
                    ).then((questionResponse) =>
                        onQuestionCreated({
                            id: questionResponse.data?.id,
                            text: answerType === "fb" ? "Fill in the blanks" : answerType === "md" ? "Match the phrases with their descriptions" : answerType === "sqc" ? "Put the sentences in the correct sequence" : request.text,
                            answerId: answerId
                        } as Question, answer)
                    )
                } catch (error) {
                    if (error instanceof ExpectationFailedError) {
                        setErrorText(error.message);
                        setShouldShowError(true);
                    } else {
                        alert(error);
                    }
                }
            })
        } else setShouldShowError(true);

    }

    return (
        <Modal show onHide={onDismiss}>
            <Modal.Header data-bs-theme="dark" closeButton>
                <Modal.Title>
                    Create Question
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {shouldShowError && <Alert variant="danger" >{errorText}</Alert>}
                <AnswerTypeSelector
                    disabled={isAnswerTypeSelectorDisabled} 
                    onAnswerTypeChange={(type) => {
                        setChoices([]);
                        setAnswerType(type);
                    }} 
                />
                {answerType === "sc" &&
                    <SingleChoiceAnswerForm
                        onChoiceAdded={(choice) => {
                            setIsAnswerTypeSelectorDisabled(true);
                            setChoices([...choices, choice])
                        }}
                        onCorrectChoiceAdded={(correctChoice) => {
                            setAnswer({
                                choices: choices,
                                correctAnswerChoice: correctChoice
                            } as SingleChoice);
                        }}
                    />
                }
                {answerType === "mc" &&
                    <MultipleChoiceAnswerForm
                        onChoiceAdded={(choice) => {
                            setIsAnswerTypeSelectorDisabled(true);
                            setChoices([...choices, choice])
                        }}
                        onCorrectChoiceAdded={(correctChoice) => {
                            setAnswer({
                                choices: choices,
                                correctAnswerChoices: !answer ? [correctChoice] : [...(answer as MultipleChoice).correctAnswerChoices, correctChoice] 
                            } as MultipleChoice);
                        }}
                    />
                }
                {answerType === "tf" &&
                    <TrueFalseAnswerForm 
                        onAnswerAdded={(isTrue) => {
                            setAnswer({
                                isTrue: isTrue
                            } as TrueFalse);
                        }} 
                    />
                }
                {answerType === "fb" &&
                    <FillTheBlanksAnswerForm onSentenceItemAdded={(sentenceItem) => {
                        setAnswer({
                            sentences: !answer ? [sentenceItem] : [...(answer as FillTheBlanks).sentences, sentenceItem],
                            positionsOfBlanks: makePositionsOfBlanks(!answer ? [sentenceItem] : [...(answer as FillTheBlanks).sentences, sentenceItem])
                        } as FillTheBlanks);
                    }} />
                }
                {answerType === "md" &&
                    <MatchDescriptionsAnswerForm onPhraseDescriptionPairAdded={(matchDescriptionsItem) => {
                        setAnswer({
                            phrases: !answer ? [matchDescriptionsItem.phrase] : [...(answer as MatchDescriptions).phrases, matchDescriptionsItem.phrase],
                            descriptions: !answer ? [matchDescriptionsItem.description] : [...(answer as MatchDescriptions).descriptions, matchDescriptionsItem.description],
                        } as MatchDescriptions);
                    }} />
                }
                {answerType === "sqc" &&
                    <SequencingAnswerForm onSequenceItemAdded={(sequenceItem) => {
                        setAnswer({
                            answerTextsInCorrectSequence: !answer ? [sequenceItem] : [...(answer as Sequencing).answerTextsInCorrectSequence, sequenceItem]
                        } as Sequencing);
                    }} />
                }
                <hr />
                <Form onSubmit={handleSubmit(onSubmit)}>
                    {(answerType !== "fb" && answerType !== "md" && answerType !== "sqc") && <TextInputField
                        name="text"
                        label="Question text"
                        type="text"
                        placeholder="Text"
                        register={register}
                        registerOptions={{ required: "Required" }}
                        error={errors.text}
                        disabled={answerType === "fb" || answerType === "md" || answerType === "sqc"}
                    />}
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        className={styleUtils.width100}>
                        Create Question
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}