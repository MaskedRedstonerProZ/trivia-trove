"use client";

import { useForm } from "react-hook-form";
import SelectInputField from "./SelectInputField";
import CreateAnswerRequest from "@/models/requests/CreateAnswerRequest";
import { Button, Form } from "react-bootstrap";

interface AnswerTypeSelectorProps {
    onAnswerTypeChange: (answerType: string) => void,
    disabled?: boolean
}

export default function AnswerTypeSelector({ onAnswerTypeChange, disabled }: AnswerTypeSelectorProps) {
    
    const { register, formState: { errors, isSubmitting } } = useForm<CreateAnswerRequest>();

    return (
        <>
        <Form>
            <SelectInputField
                name="type"
                label="Select Answer type"
                disabled={disabled}
                register={register}
                error={errors.type}
                onChange={(e) => {
                    onAnswerTypeChange(e.target.value);
                }}
            >
                <option value="sc">Single choice</option>
                <option value="mc">Multiple choice</option>
                <option value="tf">True-False</option>
                <option value="fb">Fill in the blanks</option>
                <option value="md">Matching phrases with descriptions</option>
                <option value="sqc">Sequencing</option>
            </SelectInputField>
        </Form>
        </>
    );
}