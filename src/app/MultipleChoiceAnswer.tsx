"use client";

import { useState } from "react";
import { Form } from "react-bootstrap";
import StandardButton from "./StandardButton";

interface MultipleChoiceAnswerProps {
    choices: string[],
    correctAnswerChoices: string[],
    shouldShowCorrectAnswers: boolean,
    onCorrectChoicesSelected: (correctChoices: string[]) => void
}

export default function MultipleChoiceAnswer({ choices, correctAnswerChoices, shouldShowCorrectAnswers, onCorrectChoicesSelected }: MultipleChoiceAnswerProps) {

    const [potentialCorrectAnswers, setPotentialCorrectAnswers] = useState<string[]>([]);
    const [didSubmit, setDidSubmit] = useState(false);

    function addPotentialCorrectAnswer(potentialCorrectAnswer: string) {
        setPotentialCorrectAnswers([...potentialCorrectAnswers, potentialCorrectAnswer]);
    }

    return (
        <>
            {shouldShowCorrectAnswers ? 
                choices.map((choice, i) => (
                    <Form.Check
                        checked={correctAnswerChoices.includes(choice)}
                        readOnly={correctAnswerChoices.includes(choice)}
                        label={choice}
                        name="mc-answer"
                        type="checkbox"
                        key={i}
                    />
                ))
                :
                choices.map((choice, i) => (
                    <Form.Check
                        label={choice}
                        defaultChecked={false}
                        name="mc-answer"
                        type="checkbox"
                        key={i}
                        onChange={() => {
                            addPotentialCorrectAnswer(choice);
                        }}
                    />
                ))
            }
            {!shouldShowCorrectAnswers && <center><StandardButton disabled={didSubmit} text="Submit answer" onClick={() => { onCorrectChoicesSelected(potentialCorrectAnswers); setDidSubmit(true); } } /></center>}
        </>
    );
}